﻿using System;
using System.Linq;
using System.Web.Mvc;
using KPMG.Enums;

namespace KPMG.DAL
{
    public class ControlsHelpers
    {
        private CompanyContext db = new CompanyContext();

        public string[] GetSuggestions()
        {
            return db.Employees.ToList().Select(p => p.Fullname).ToArray();
        }

        public SelectList GetDepartmentsList()
        {
            return new SelectList(db.Departments.Select(p => p.Name).Distinct());
        }

        public SelectList GetGradesList()
        {
            var gradesList = Enum.GetNames(typeof(Grade)).ToList();
            return new SelectList(gradesList);
        }

        public SelectList GetPerformanceManagerList()
        {
            return new SelectList(db.Employees.Select(p => p.PerformanceManager).Distinct().ToList()
                .Select(p => p != null ? p.Fullname : "None"));
        }
    }
}