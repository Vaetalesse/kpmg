﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using KPMG.DAL;
using KPMG.Enums;
using KPMG.Models;

namespace KPMG.DAL
{
    public class CompanyInitializer : DropCreateDatabaseAlways<CompanyContext>
    {
        protected override void Seed(CompanyContext context)
        {
            var departments = new List<Department>
            {
                new Department {Name = "Finance Department" },
                new Department {Name = "IT Department" },
                new Department {Name = "Human Resources Department" },
                new Department {Name = "Customer Service Department" },
            };

            foreach (var department in departments)
            {
                context.Departments.AddOrUpdate(department);
            }

            context.SaveChanges();

            var employees = new List<Employee>
            {
                new Employee { FirstName = "Jakub", LastName = "Lubaszewski", HireDate = DateTime.Parse("2011-09-15"), Department = departments.Single(d => d.Name == "IT Department"), Grade = Grade.A },
                new Employee { FirstName = "Robert", LastName = "Dawkins", HireDate = DateTime.Parse("2009-02-01"), Department = departments.Single(d => d.Name == "IT Department"), Grade = Grade.B },
                new Employee { FirstName = "Jaques", LastName = "Robespierre", HireDate = DateTime.Parse("2012-01-25"), Department = departments.Single(d => d.Name == "IT Department"), Grade = Grade.D },
                new Employee { FirstName = "Patrick", LastName = "Sponge", HireDate = DateTime.Parse("2012-03-01"), Department = departments.Single(d => d.Name == "IT Department"), Grade = Grade.C },
                new Employee { FirstName = "Maria", LastName = "Lozanno", HireDate = DateTime.Parse("2006-11-12"), Department = departments.Single(d => d.Name == "Finance Department"), Grade = Grade.B },
                new Employee { FirstName = "Gustav", LastName = "Letho", HireDate = DateTime.Parse("2010-04-02"), Department = departments.Single(d => d.Name == "Finance Department"), Grade = Grade.E },
                new Employee { FirstName = "Louie", LastName = "Gryphin", HireDate = DateTime.Parse("2007-05-04"), Department = departments.Single(d => d.Name == "Finance Department"), Grade = Grade.C },
                new Employee { FirstName = "Johnny", LastName = "Begut", HireDate = DateTime.Parse("2006-01-02"), Department = departments.Single(d => d.Name == "Finance Department"), Grade = Grade.A },
                new Employee { FirstName = "Edmundo", LastName = "Dantes", HireDate = DateTime.Parse("2009-12-01"), Department = departments.Single(d => d.Name == "Human Resources Department"), Grade = Grade.B },
                new Employee { FirstName = "Fernand", LastName = "Mondego", HireDate = DateTime.Parse("2010-04-01"), Department = departments.Single(d => d.Name == "Human Resources Department"), Grade = Grade.F },
                new Employee { FirstName = "Renee", LastName = "Villefort", HireDate = DateTime.Parse("2007-10-03"), Department = departments.Single(d => d.Name == "Human Resources Department"), Grade = Grade.D },
                new Employee { FirstName = "Alice", LastName = "Cooper", HireDate = DateTime.Parse("2008-08-28"), Department = departments.Single(d => d.Name == "Customer Service Department"), Grade = Grade.C },
                new Employee { FirstName = "Anna", LastName = "Antonina", HireDate = DateTime.Parse("2012-12-31"), Department = departments.Single(d => d.Name == "Customer Service Department"), Grade = Grade.C },
            };

            employees.Single(e => e.LastName == "Dawkins").PerformanceManager = employees.Single(e => e.LastName == "Lubaszewski");
            employees.Single(e => e.LastName == "Robespierre").PerformanceManager = employees.Single(e => e.LastName == "Dawkins");
            employees.Single(e => e.LastName == "Sponge").PerformanceManager = employees.Single(e => e.LastName == "Dawkins");
            employees.Single(e => e.LastName == "Letho").PerformanceManager = employees.Single(e => e.LastName == "Lozanno");
            employees.Single(e => e.LastName == "Gryphin").PerformanceManager = employees.Single(e => e.LastName == "Lozanno");
            employees.Single(e => e.LastName == "Begut").PerformanceManager = employees.Single(e => e.LastName == "Lozanno");
            employees.Single(e => e.LastName == "Mondego").PerformanceManager = employees.Single(e => e.LastName == "Dantes");
            employees.Single(e => e.LastName == "Villefort").PerformanceManager = employees.Single(e => e.LastName == "Mondego");

            foreach (var employee in employees)
            {
                context.Employees.AddOrUpdate(employee);
            }

            context.SaveChanges();

        }
    }
}