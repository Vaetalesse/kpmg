﻿using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using KPMG.Models;

namespace KPMG.DAL
{
    public class Filters
    {
        private readonly CompanyContext _db = new CompanyContext();

        public IQueryable<Employee> GetAllEmployees()
        {
                var employees = from e in _db.Employees
                    select e;
                return employees.Include(d => d.Department);
        }

        public IQueryable<Employee> NameFilter(string name)
        {
            if (!string.IsNullOrEmpty(name))
            {
                var wordsArray = name.Split();
                var employees = from e in _db.Employees
                    where wordsArray.Contains(e.FirstName) || wordsArray.Contains(e.LastName)
                    select e;
                return employees.Include(d => d.Department).Include(d => d.PerformanceManager);
            }
            return GetAllEmployees();
        }

        public IQueryable<Employee> HireDateFilter(string hireDate)
        {
            if (!string.IsNullOrEmpty(hireDate))
            {
                var parsedDate = DateTime.Parse(hireDate);
                var employees = from e in _db.Employees
                    where e.HireDate > parsedDate
                    select e;
                return employees.Include(d => d.Department).Include(d => d.PerformanceManager);
            }
            return GetAllEmployees();
        }

        public IQueryable<Employee> DepartmentFilter(string departmentName)
        {
            if (departmentName != "Any")
            {
                var employees = from e in _db.Employees
                    where e.Department.Name == departmentName
                    select e;
                return employees.Include(d => d.Department).Include(d => d.PerformanceManager);
            }
            return GetAllEmployees();
        }

        public IQueryable<Employee> GradeFilter(string grade)
        {
            if (grade != "Any")
            {
                var employees = from e in _db.Employees
                    where e.Grade.ToString() == grade
                    select e;
                return employees.Include(d => d.Department).Include(d => d.PerformanceManager);
            }
            return GetAllEmployees();
        }

        public IQueryable<Employee> ManagerFilter(string managerName)
        {
            if (managerName == "None")
            {
                var employees = from e in _db.Employees
                    where e.PerformanceManager == null
                    select e;
                return employees.Include(d => d.Department).Include(d => d.PerformanceManager);
            }
            else if (managerName != "Any")
            {
                var employees = from e in _db.Employees
                    where e.PerformanceManager.LastName + " " + e.PerformanceManager.FirstName == managerName
                    select e;
                return employees.Include(d => d.Department).Include(d => d.PerformanceManager);
            }
            return GetAllEmployees();
        }

    }
}