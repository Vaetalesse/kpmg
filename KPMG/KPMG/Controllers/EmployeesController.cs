﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using KPMG.DAL;
using KPMG.Models;

namespace KPMG.Controllers
{
    public class EmployeesController : Controller
    {
        private readonly Filters _filters = new Filters();
        private readonly ControlsHelpers _controlHelper = new ControlsHelpers();

        // GET: Employees
        public ActionResult Index()
        {
            var employees = _filters.GetAllEmployees();

            ViewBag.departmentsList = _controlHelper.GetDepartmentsList();
            ViewBag.gradesList = _controlHelper.GetGradesList();
            ViewBag.perfManList = _controlHelper.GetPerformanceManagerList();

            return View(employees.ToList());
        }

        [HttpPost]
        public ActionResult FilterResults(string filterId, string value)
        {
            List<Employee> employeesList;
            switch (filterId)
            {
                case "hireDateFilter":
                    employeesList = _filters.HireDateFilter(value).ToList();
                    break;
                case "departmentsList":
                    employeesList = _filters.DepartmentFilter(value).ToList();
                    break;
                case "gradesList":
                    employeesList = _filters.GradeFilter(value).ToList();
                    break;
                case "perfManList":
                    employeesList = _filters.ManagerFilter(value).ToList();
                    break;
                default:
                    employeesList = _filters.NameFilter(value).ToList();
                    break;
            }
            return PartialView("Filter", employeesList);
        }

        [HttpPost]
        public JsonResult Autocompleter()
        {
            var suggestions = _controlHelper.GetSuggestions();
            return Json(suggestions);
        }
    }
}
