﻿using System.Web.Mvc;

namespace KPMG.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult About()
        {
            ViewBag.Message = "Application description";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Developer information";

            return View();
        }
    }
}