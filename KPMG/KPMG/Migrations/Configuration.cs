namespace KPMG.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using DAL;
    using Models;
    using Enums;
    using System.Collections.Generic;

    internal sealed class Configuration : DbMigrationsConfiguration<CompanyContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(CompanyContext context)
        {
        }
    }
}
