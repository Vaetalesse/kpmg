﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KPMG.Enums
{
    public enum Grade
    {
        A,
        B,
        C,
        D,
        E,
        F
    }
}