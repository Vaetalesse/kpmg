﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KPMG.Enums;
using System.ComponentModel.DataAnnotations;

namespace KPMG.Models
{
    public class Employee
    {
        public int Id { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required, DataType(DataType.Date), Display(Name = "Hire date"), DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime HireDate { get; set; }
        [Required]
        public Department Department { get; set; }
        [Required]
        public Grade Grade { get; set; }
        [Display(Name = "Performance Manager")]
        public Employee PerformanceManager { get; set; }

        [Display(Name = "Full Name")]
        public string Fullname => LastName + " " + FirstName;
    }
}