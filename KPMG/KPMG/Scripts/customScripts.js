﻿$("#nameFilter").change(function () {
    var inputValue = $('#nameFilter').val();
    var filter = "nameFilter";
    sendRequest(filter, inputValue);
    clearFilter(filter);
});

$("#hireDateFilter").change(function () {
    var inputValue = $('#hireDateFilter').val();
    var filter = "hireDateFilter";
    sendRequest(filter, inputValue);
    clearFilter(filter);
});

$("#departmentsList").change(function () {
    var listValue = $('#departmentsList :selected').text();
    var filter = "departmentsList";
    sendRequest(filter, listValue);
    clearFilter(filter);
});

$("#gradesList").change(function () {
    var listValue = $('#gradesList :selected').text();
    var filter = "gradesList";
    sendRequest(filter, listValue);
    clearFilter(filter);
});

$("#perfManList").change(function () {
    var listValue = $('#perfManList :selected').text();
    var filter = "perfManList";
    sendRequest(filter, listValue);
    clearFilter(filter);
});

function clearFilter(filterId) {
    if (filterId !== "nameFilter") {
        document.getElementById("nameFilter").value = "";
    }
    if (filterId !== "hireDateFilter") {
        document.getElementById("hireDateFilter").value = "";
    }
    if (filterId !== "departmentsList") {
        document.getElementById("departmentsList").selectedIndex = 0;
    }
    if (filterId !== "gradesList") {
        document.getElementById("gradesList").selectedIndex = 0;
    }
    if (filterId !== "perfManList") {
        document.getElementById("perfManList").selectedIndex = 0;
    }
}

function showFilters() {
    if (document.getElementById("filters").style.display === "none") {
        document.getElementById("filters").style.display = "block";
        document.getElementById("filterButton").innerHTML = "Hide filters";
    } else {
        document.getElementById("filters").style.display = "none";
        document.getElementById("filterButton").innerHTML = "Show filters";
    }
}

$(document).ready(function() {
    $.ajax({
            method: "POST",
            url: "/Employees/Autocompleter",
        })
        .done(function(data) {
            $("#nameFilter").autocomplete({
                minLength: 2,
                source: data
            });
        })
        .fail(function() {
            alert("Couldn't retrieve data from database");
        });
});

function sendRequest(filterId, filterValue) {
    var container = $("#contentSection");
    spinner.spin(target);
    $.ajax({
        method: "POST",
        url: "/Employees/FilterResults",
        data: { filterId: filterId, value: filterValue }
    })
        .done(function (data) {
            container.html(data);
        })
        .fail(function() {
            alert("Couldn't retrieve data from database");
        })
    .always(function () {
        spinner.stop();
    });
}

var opts = {
    lines: 12             // The number of lines to draw
    , length: 7             // The length of each line
    , width: 5              // The line thickness
    , radius: 10            // The radius of the inner circle
    , scale: 2.0            // Scales overall size of the spinner
    , corners: 1            // Roundness (0..1)
    , color: '#000'         // #rgb or #rrggbb
    , opacity: 1 / 4          // Opacity of the lines
    , rotate: 0             // Rotation offset
    , direction: 1          // 1: clockwise, -1: counterclockwise
    , speed: 1              // Rounds per second
    , trail: 100            // Afterglow percentage
    , fps: 20               // Frames per second when using setTimeout()
    , zIndex: 2e9           // Use a high z-index by default
    , className: 'spinner'  // CSS class to assign to the element
    , top: '25%'            // center vertically
    , left: '50%'           // center horizontally
    , shadow: false         // Whether to render a shadow
    , hwaccel: false        // Whether to use hardware acceleration (might be buggy)
    , position: 'absolute'  // Element positioning
}
var target = document.getElementById('filters');
var spinner = new Spinner(opts);
