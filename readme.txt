To start application locally you must do following steps:
1) Download repository from BitBucket: https://Vaetalesse@bitbucket.org/Vaetalesse/kpmg.git
2) Database's backup is in "db" folder, restore it using SQL Server Management Studio
3) Using Visual Studio open solution file KPMG.sln and alter connectionString in connectionStrings section so "Server=..." parameter fits your database engine name
4) Build the solution
5) Press F5 or click "Start debugging" in Debug menu to start application  